<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JSDoc: Source: graph/graphTraversal.js</title>

    <script src="scripts/prettify/prettify.js"> </script>
    <script src="scripts/prettify/lang-css.js"> </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="styles/prettify-tomorrow.css">
    <link type="text/css" rel="stylesheet" href="styles/jsdoc-default.css">
</head>

<body>

<div id="main">

    <h1 class="page-title">Source: graph/graphTraversal.js</h1>

    


    
    <section>
        <article>
            <pre class="prettyprint source linenums"><code>/**
 * Constructs a GraphTraversal
 * 
 * @classdesc
 * 
 * &lt;p>
 * A GraphTraversal defines a strategy for traversing an {@link AbstractGraph}.
 * &lt;/p>
 * 
 * &lt;p>
 * The two key components of GraphTraversal are a set of states and a set of
 * {@link GraphTraversal~VisitFunction VisitFunctions}, both of which the user
 * of this class must define. States are Strings that indicate the state of the
 * current traversal. VisitFunctions perform arbitrary operations given the
 * current node the GraphTraversal is visiting.
 * &lt;/p>
 * 
 * &lt;p>
 * You can associate visitFunctions with states so that different VisitFunctions
 * are invoked when the traversal is in different states.
 * &lt;/p>
 * 
 * &lt;p>
 * Consider the following example: You want to traverse from head towards tail
 * the nodes of host "a" until you find a node n that communicates with host
 * "b". After that, you want to traverse from n to head the nodes of host "b".
 * There might be two states: "on a" and "on b". The VisitFunctions for the two
 * states might be as follows:
 * &lt;/p>
 * 
 * &lt;pre>
 * 'on a':
 * function(graphTraversal, node, state, data) {
 *     if(node.getChildByHost('b') != null) {
 *         graphTraversal.setCurrentNode(node.getChildByHost('b'));
 *         graphTraversal.setState('on b');
 *     }
 *     else {
 *         graphTraversal.setCurrentNode(node.getNext());
 *     }
 * }
 * 
 * 'on b':
 * function(graphTraversal, node, state, data) {
 *     if(!node.isHead()) {
 *         graphTraversal.setCurrentNode(node.getPrev());
 *     }
 * }
 * &lt;/pre>
 * 
 * &lt;p>
 * Alternately, you could chose to only define a defaultVisitFunction and decide
 * what should be done based on the state variable
 * &lt;/p>
 * 
 * @constructor
 */
function GraphTraversal() {

    /** @private */
    this.visitFunctions = {};

    /** @private */
    this.defaultVisitFunction = null;

    /** @private */
    this.state = null;

    /** @private */
    this.currentNode = null;

    /** @private */
    this.currentData = null;

    /** @private */
    this._hasEnded = false;

    this.reset();
}

/**
 * Resets the state of the traversal. Bound visit functions will not be touched.
 */
GraphTraversal.prototype.reset = function() {
    this.state = null;

    this.currentNode = null;

    this.currentData = null;

    this.parent = {};

    this._hasEnded = false;
};

/**
 * A VisitFunction is invoked each time the GraphTraversal encounters a new
 * node. Which VisitFunction is invoked depends on what state the GraphTraversal
 * is in.
 * 
 * @callback GraphTraversal~VisitFunction
 * @param {GraphTraversal} gt The GraphTraversal object invoking the function
 * @param {AbstractNode} node The current node being visited
 * @param {String} state The current state
 * @param {Object} data The current data object.
 * @see {@link GraphTraversal#step}
 */

/**
 * Sets the visit function for a state. The visit function will be invoked when
 * the GraphTraversal is in the given state. If there was previously a visit
 * function already defined for the state, it will be replaced.
 * 
 * @param {String} state The state
 * @param {GraphTraversal~visitFunction} fn The visit function
 */
GraphTraversal.prototype.setVisitFunction = function(state, fn) {
    this.visitFunctions[state] = fn;
};

/**
 * Sets the default visit function. The default visit function will be invoked
 * if there is no other visit function bound to the current state, OR if the
 * current state is null.
 * 
 * @param {GraphTraversal~visitFunction} fn The visit function to set as default
 */
GraphTraversal.prototype.setDefaultVisitFunction = function(fn) {
    this.defaultVisitFunction = fn;
};

/**
 * Sets the current state
 * 
 * @param {String} state
 */
GraphTraversal.prototype.setState = function(state) {
    this.state = state;
};

/**
 * Sets the current node - the node that will be visited next.
 * 
 * @param {AbstractNode} node
 */
GraphTraversal.prototype.setCurrentNode = function(node) {
    this.currentNode = node;
};

/**
 * Sets the current data. The current data is an object that will be passed to
 * the visit function the next time it's invoked. It can be used to pass
 * arbitrary data
 * 
 * @param {*} data
 */
GraphTraversal.prototype.setCurrentData = function(data) {
    this.currentData = data;
};

/**
 * Ends the traversal. After this method is invoked, no more nodes will be
 * visited.
 */
GraphTraversal.prototype.end = function() {
    this._hasEnded = true;
};

/**
 * Determines if this traversal has ended
 * 
 * @returns {Boolean} true is this traversal has ended
 */
GraphTraversal.prototype.hasEnded = function() {
    return this._hasEnded;
};

/**
 * &lt;p>
 * Executes a single step of the traversal. The correct
 * {@link GraphTraversal~VisitFunction VisitFunction} is fetched and is
 * executed. If there is a VisitFunction associated with the current state (i.e
 * via a previous call to {@link setVisitFunction}) and the current state is
 * not null, then that VisitFunction is invoked. Otherwise, the default visit
 * function is invoked. Whatever is returned by the VisitFunction will be
 * returned by this method
 * &lt;/p>
 * 
 * &lt;p>
 * If this traversal has already {@link GraphTraversal#end end}ed, then this
 * method does nothing and returns null
 * &lt;/p>
 * 
 * @returns {*} The return value of the VisitFunction if one was executed, or
 *          null otherwise
 * @see {@link GraphTraversal#setVisitFunction}
 * @see {@link GraphTraversal#setDefaultVisitFunction}
 */
GraphTraversal.prototype.step = function() {
    if (this.hasEnded()) {
        return null;
    }

    return this.stepInner();
};

/**
 * This protected function is what actually executes the step.
 * {@link GraphTraversal#step} performs some validation and then calls this
 * function. Typically, classes extending this class will override this method
 * to specify what happens during each "step"
 * 
 * @protected
 * @returns {*} The return value of the VisitFunction if one was executed, or
 *          null otherwise
 * @see {@link GraphTraversal#setVisitFunction}
 * @see {@link GraphTraversal#setDefaultVisitFunction}
 */
GraphTraversal.prototype.stepInner = function() {
    if (this.state == null || !this.visitFunctions[this.state]) {
        if (this.defaultVisitFunction == null) {
            throw new Exception("GraphTraversal.prototype.step: no valid visit function");
        }
        return this.defaultVisitFunction(this, this.currentNode, this.state, this.currentData);
    }
    else {
        return this.visitFunctions[this.state](this, this.currentNode, this.state, this.currentData);
    }
};

/**
 * "Runs" the traversal. The traversal will be continuously stepped though (i.e
 * with step()) until the traversal has ended. The value returned by the last
 * call to {@link GraphTraversal#step step} will be returned by this method.
 * 
 * @returns {*} The value returned by the last call to step, or null if step was
 *          never invoked.
 */
GraphTraversal.prototype.run = function() {
    var ret = null;
    while (!this.hasEnded()) {
        ret = this.step();
    }
    return ret;

};
</code></pre>
        </article>
    </section>




</div>

<nav>
    <h2><a href="index.html">Index</a></h2><h3>Classes</h3><ul><li><a href="AbstractGraph.html">AbstractGraph</a></li><li><a href="AbstractNode.html">AbstractNode</a></li><li><a href="AddFamilyEvent.html">AddFamilyEvent</a></li><li><a href="AddNodeEvent.html">AddNodeEvent</a></li><li><a href="AST.html">AST</a></li><li><a href="BinaryOp.html">BinaryOp</a></li><li><a href="BroadcastGatherFinder.html">BroadcastGatherFinder</a></li><li><a href="BuilderGraph.html">BuilderGraph</a></li><li><a href="BuilderNode.html">BuilderNode</a></li><li><a href="ChangeEvent.html">ChangeEvent</a></li><li><a href="CollapseSequentialNodesTransformation.html">CollapseSequentialNodesTransformation</a></li><li><a href="Controller.html">Controller</a></li><li><a href="CustomMotifFinder.html">CustomMotifFinder</a></li><li><a href="DFSGraphTraversal.html">DFSGraphTraversal</a></li><li><a href="Exception.html">Exception</a></li><li><a href="Global_.html">Global</a></li><li><a href="GraphBuilder.html">GraphBuilder</a></li><li><a href="GraphBuilderHost.html">GraphBuilderHost</a></li><li><a href="GraphBuilderNode.html">GraphBuilderNode</a></li><li><a href="GraphTraversal.html">GraphTraversal</a></li><li><a href="HideHostTransformation.html">HideHostTransformation</a></li><li><a href="HighlightHostTransformation.html">HighlightHostTransformation</a></li><li><a href="HighlightMotifTransformation.html">HighlightMotifTransformation</a></li><li><a href="HostPermutation.html">HostPermutation</a></li><li><a href="Identifier.html">Identifier</a></li><li><a href="ImplicitSearch.html">ImplicitSearch</a></li><li><a href="Layout.html">Layout</a></li><li><a href="LEMInterpreter.html">LEMInterpreter</a></li><li><a href="LEMInterpreterValue.html">LEMInterpreterValue</a></li><li><a href="LEMParser.html">LEMParser</a></li><li><a href="LEMTokenizer.html">LEMTokenizer</a></li><li><a href="LengthPermutation.html">LengthPermutation</a></li><li><a href="LogEvent.html">LogEvent</a></li><li><a href="LogEventMatcher.html">LogEventMatcher</a></li><li><a href="LogOrderPermutation.html">LogOrderPermutation</a></li><li><a href="LogParser.html">LogParser</a></li><li><a href="ModelGraph.html">ModelGraph</a></li><li><a href="ModelNode.html">ModelNode</a></li><li><a href="Motif.html">Motif</a></li><li><a href="MotifFinder.html">MotifFinder</a></li><li><a href="MotifGroup.html">MotifGroup</a></li><li><a href="MotifNavigator.html">MotifNavigator</a></li><li><a href="MotifNavigatorData.html">MotifNavigatorData</a></li><li><a href="NamedRegExp.html">NamedRegExp</a></li><li><a href="RegexLiteral.html">RegexLiteral</a></li><li><a href="RemoveFamilyEvent.html">RemoveFamilyEvent</a></li><li><a href="RemoveHostEvent.html">RemoveHostEvent</a></li><li><a href="RemoveNodeEvent.html">RemoveNodeEvent</a></li><li><a href="RequestResponseFinder.html">RequestResponseFinder</a></li><li><a href="SearchBar.html">SearchBar</a></li><li><a href="Shiviz.html">Shiviz</a></li><li><a href="ShowDiffTransformation.html">ShowDiffTransformation</a></li><li><a href="SpaceTimeLayout.html">SpaceTimeLayout</a></li><li><a href="StringLiteral.html">StringLiteral</a></li><li><a href="TextQueryMotifFinder.html">TextQueryMotifFinder</a></li><li><a href="Token.html">Token</a></li><li><a href="TokenType.html">TokenType</a></li><li><a href="Transformation.html">Transformation</a></li><li><a href="Transformer.html">Transformer</a></li><li><a href="Util.html">Util</a></li><li><a href="VectorTimestamp.html">VectorTimestamp</a></li><li><a href="VectorTimestampSerializer.html">VectorTimestampSerializer</a></li><li><a href="View.html">View</a></li><li><a href="VisualEdge.html">VisualEdge</a></li><li><a href="VisualGraph.html">VisualGraph</a></li><li><a href="VisualNode.html">VisualNode</a></li></ul><h3>Global</h3><ul><li><a href="global.html#Line">Line</a></li></ul>
</nav>

<br clear="both">

<footer>
    Documentation generated by <a href="https://github.com/jsdoc3/jsdoc">JSDoc 3.3.0-dev</a> on Sun Apr 26 2015 22:55:54 GMT-0700 (PDT)
</footer>

<script> prettyPrint(); </script>
<script src="scripts/linenumber.js"> </script>
</body>
</html>
