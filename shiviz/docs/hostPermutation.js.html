<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JSDoc: Source: visualization/hostPermutation.js</title>

    <script src="scripts/prettify/prettify.js"> </script>
    <script src="scripts/prettify/lang-css.js"> </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="styles/prettify-tomorrow.css">
    <link type="text/css" rel="stylesheet" href="styles/jsdoc-default.css">
</head>

<body>

<div id="main">

    <h1 class="page-title">Source: visualization/hostPermutation.js</h1>

    


    
    <section>
        <article>
            <pre class="prettyprint source linenums"><code>/**
 * The constructor for this abstract class will typically be invoked by concrete
 * sub-classes
 * 
 * @clasdesc
 * 
 * &lt;p>
 * A HostPermutation is responsible for determining the order in which hosts
 * should be displayed in the visualization of the graph. This class is also
 * responsible for determining the hosts' visualization's color. This is because
 * Shiviz always uses colors in a fixed order for consistency, so host color is
 * tied to host permutation.
 * &lt;/p>
 * 
 * &lt;p>
 * Typical usage involves adding hosts to order using
 * {@link HostPermutation#addGraph}, calling {@link HostPermutation#update} to
 * compute the ordering of hosts, and then using one of the getters to retrieve
 * the computed host color and order.
 * &lt;/p>
 * 
 * &lt;p>
 * HostPermutation and all its subclasses must ensure that the computed host
 * order and colors do not change unless {@link HostPermutation#update} is
 * called. Before the very first time update is called, do not try to retrieve
 * the computed host color and order (i.e by using
 * {@link HostPermutation#getHosts}, etc)
 * &lt;/p>
 * 
 * &lt;p>
 * HostPermutation is an abstract class. To implement a specific permutation, a
 * class that extends HostPermutation should be written and the
 * {@link HostPermutation#update} method should be overriden.
 * &lt;/p>
 * 
 * @constructor
 * @abstract
 * @param {Boolean} reverse If true, the ordering of hosts is reversed
 */
function HostPermutation(reverse) {

    if (this.constructor == HostPermutation) {
        throw new Exception("Cannot instantiate HostPermutation; HostPermutation is an abstract class");
    }

    /** @private */
    this.graphs = [];

    /** @private */
    this.hosts = [];

    /** @private */
    this.reverse = reverse;

    /** @private */
    this.color = d3.scale.category20();

    /** @private */
    this.hostColors = {};

    /** @private */
    this.hasUpdated = false;
}

/**
 * Adds a graph to the HostPermutation. HostPermutation determines an ordering
 * of hosts based on the hosts of graphs added using this method
 * 
 * @param {ModelGraph} graph The graph to add
 */
HostPermutation.prototype.addGraph = function(graph) {
    this.graphs.push(graph);
};

/**
 * Gets a list of hosts in the order determined by the HostPermutation. Note
 * that you must call {@link update} to compute the ordering of hosts before
 * retrieving it with this method
 * 
 * @returns {Array&lt;String>} Array of hosts
 */
HostPermutation.prototype.getHosts = function() {
    if (!this.hasUpdated) {
        throw new Exception("HostPermutation.prototype.getHosts: You must call update() first");
    }
    return this.reverse ? this.hosts.slice().reverse() : this.hosts.slice();
};

/**
 * Gets a list of hosts in the order determined by the HostPermutation. Only
 * hosts contained in both this HostPermutation and the filter array will be
 * returned. Note that you must call {@link update} to compute the ordering of
 * hosts before retrieving it with this method.
 * 
 * @param {Array&lt;String>} filter The filter array.
 * @returns {Array&lt;String>} Array of hosts
 */
HostPermutation.prototype.getHostsAndFilter = function(filter) {
    if (!this.hasUpdated) {
        throw new Exception("HostPermutation.prototype.getHosts: You must call update() first");
    }

    var filterSet = {};
    for (var i = 0; i &lt; filter.length; i++) {
        filterSet[filter[i]] = true;
    }

    var ret = [];
    for (var i = 0; i &lt; this.hosts.length; i++) {
        if (filterSet[this.hosts[i]]) {
            ret.push(this.hosts[i]);
        }
    }

    if (this.reverse) {
        ret.reverse();
    }

    return ret;
};

/**
 * Gets the designated color of the specified host
 * 
 * @param {String} host The host whose color you want to retrieve
 * @returns {String} A valid color string.
 */
HostPermutation.prototype.getHostColor = function(host) {
    if (!this.hasUpdated) {
        throw new Exception("HostPermutation.prototype.getHosts: You must call update() first");
    }

    return this.hostColors[host];
};

/**
 * Returns all designated host colors
 * 
 * @returns {Object&lt;String, String>} A mapping of host name to host color
 */
HostPermutation.prototype.getHostColors = function() {
    if (!this.hasUpdated) {
        throw new Exception("HostPermutation.prototype.getHosts: You must call update() first");
    }

    var colors = {};
    for (var host in this.hostColors) {
        colors[host] = this.hostColors[host];
    }
    return colors;
};

/**
 * &lt;p>
 * The update method alone is responsible for figuring out the ordering of hosts
 * and for assigning host colors.
 * &lt;/p>
 * 
 * &lt;p>
 * In its current form, because it is an abstract method, it only performs color
 * assignment. Classes that extend HostPermutation must be sure to override this
 * method and extend it with host permutation assignment functionality.
 * &lt;/p>
 * 
 * @abstract
 */
HostPermutation.prototype.update = function() {

    this.hasUpdated = true;

    for (var i = 0; i &lt; this.graphs.length; i++) {
        var graph = this.graphs[i];
        var hosts = graph.getHosts();

        for (var j = 0; j &lt; hosts.length; j++) {
            var host = hosts[j];
            if (!this.hostColors[host]) {
                this.hostColors[host] = this.color(host);
            }
        }
    }

};

/**
 * Constructs a LengthPermutation
 * 
 * @classdesc
 * 
 * LengthPermutation arranges hosts in ascending order based on the number of
 * LogEvents the host contains.
 * 
 * @constructor
 * @extends HostPermutation
 * @param {Boolean} reverse If true, the ordering of hosts is reversed
 */
function LengthPermutation(reverse) {
    HostPermutation.call(this, reverse);
}

// LengthPermutation extends HostPermutation
LengthPermutation.prototype = Object.create(HostPermutation.prototype);
LengthPermutation.prototype.constructor = LengthPermutation;

/**
 * Overrides {@link HostPermutation#update}
 */
LengthPermutation.prototype.update = function() {

    HostPermutation.prototype.update.call(this);

    var currHosts = {};

    for (var i = 0; i &lt; this.graphs.length; i++) {
        var graph = this.graphs[i];
        var hosts = graph.getHosts();

        for (var j = 0; j &lt; hosts.length; j++) {
            var host = hosts[j];
            var curr = graph.getHead(host).getNext();
            var count = 0;
            while (!curr.isTail()) {
                count++;
                curr = curr.getNext();
            }

            if (!currHosts[host] || count > currHosts[host].count) {
                currHosts[host] = {
                    count: count,
                    host: host
                };
            }

        }
    }

    var hostArray = [];
    for (var key in currHosts) {
        hostArray.push(currHosts[key]);
    }

    hostArray.sort(function(a, b) {
        return a.count - b.count;
    });

    this.hosts = [];
    for (var i = 0; i &lt; hostArray.length; i++) {
        this.hosts.push(hostArray[i].host);
    }

};

/**
 * Constructs a LogOrderPermutation
 * 
 * @classdesc
 * 
 * LogOrderPermutation orders hosts based on the order they appear in logs.
 * 
 * @constructor
 * @extends HostPermutation
 * @param {Boolean} reverse If true, the ordering of hosts is reversed
 */
function LogOrderPermutation(reverse) {
    HostPermutation.call(this, reverse);

    /** @private */
    this.logs = [];
}

// LogOrderPermutation extends HostPermutation
LogOrderPermutation.prototype = Object.create(HostPermutation.prototype);
LogOrderPermutation.prototype.constructor = LogOrderPermutation;

/**
 * Adds logs. This class will order hosts based on the order they appear in the
 * logs provided.
 * 
 * @param {Array&lt;LogEvent>} logs The logs to add
 */
LogOrderPermutation.prototype.addLogs = function(logs) {
    this.logs = this.logs.concat(logs);
};

/**
 * Overrides {@link HostPermutation#update}
 */
LogOrderPermutation.prototype.update = function() {

    HostPermutation.prototype.update.call(this);

    var hostSet = {};

    for (var i = 0; i &lt; this.logs.length; i++) {
        var log = this.logs[i];
        if (!hostSet[log.getHost()]) {
            hostSet[log.getHost()] = true;
            this.hosts.push(log.getHost());
        }
    }
};
</code></pre>
        </article>
    </section>




</div>

<nav>
    <h2><a href="index.html">Index</a></h2><h3>Classes</h3><ul><li><a href="AbstractGraph.html">AbstractGraph</a></li><li><a href="AbstractNode.html">AbstractNode</a></li><li><a href="AddFamilyEvent.html">AddFamilyEvent</a></li><li><a href="AddNodeEvent.html">AddNodeEvent</a></li><li><a href="AST.html">AST</a></li><li><a href="BinaryOp.html">BinaryOp</a></li><li><a href="BroadcastGatherFinder.html">BroadcastGatherFinder</a></li><li><a href="BuilderGraph.html">BuilderGraph</a></li><li><a href="BuilderNode.html">BuilderNode</a></li><li><a href="ChangeEvent.html">ChangeEvent</a></li><li><a href="CollapseSequentialNodesTransformation.html">CollapseSequentialNodesTransformation</a></li><li><a href="Controller.html">Controller</a></li><li><a href="CustomMotifFinder.html">CustomMotifFinder</a></li><li><a href="DFSGraphTraversal.html">DFSGraphTraversal</a></li><li><a href="Exception.html">Exception</a></li><li><a href="Global_.html">Global</a></li><li><a href="GraphBuilder.html">GraphBuilder</a></li><li><a href="GraphBuilderHost.html">GraphBuilderHost</a></li><li><a href="GraphBuilderNode.html">GraphBuilderNode</a></li><li><a href="GraphTraversal.html">GraphTraversal</a></li><li><a href="HideHostTransformation.html">HideHostTransformation</a></li><li><a href="HighlightHostTransformation.html">HighlightHostTransformation</a></li><li><a href="HighlightMotifTransformation.html">HighlightMotifTransformation</a></li><li><a href="HostPermutation.html">HostPermutation</a></li><li><a href="Identifier.html">Identifier</a></li><li><a href="ImplicitSearch.html">ImplicitSearch</a></li><li><a href="Layout.html">Layout</a></li><li><a href="LEMInterpreter.html">LEMInterpreter</a></li><li><a href="LEMInterpreterValue.html">LEMInterpreterValue</a></li><li><a href="LEMParser.html">LEMParser</a></li><li><a href="LEMTokenizer.html">LEMTokenizer</a></li><li><a href="LengthPermutation.html">LengthPermutation</a></li><li><a href="LogEvent.html">LogEvent</a></li><li><a href="LogEventMatcher.html">LogEventMatcher</a></li><li><a href="LogOrderPermutation.html">LogOrderPermutation</a></li><li><a href="LogParser.html">LogParser</a></li><li><a href="ModelGraph.html">ModelGraph</a></li><li><a href="ModelNode.html">ModelNode</a></li><li><a href="Motif.html">Motif</a></li><li><a href="MotifFinder.html">MotifFinder</a></li><li><a href="MotifGroup.html">MotifGroup</a></li><li><a href="MotifNavigator.html">MotifNavigator</a></li><li><a href="MotifNavigatorData.html">MotifNavigatorData</a></li><li><a href="NamedRegExp.html">NamedRegExp</a></li><li><a href="RegexLiteral.html">RegexLiteral</a></li><li><a href="RemoveFamilyEvent.html">RemoveFamilyEvent</a></li><li><a href="RemoveHostEvent.html">RemoveHostEvent</a></li><li><a href="RemoveNodeEvent.html">RemoveNodeEvent</a></li><li><a href="RequestResponseFinder.html">RequestResponseFinder</a></li><li><a href="SearchBar.html">SearchBar</a></li><li><a href="Shiviz.html">Shiviz</a></li><li><a href="ShowDiffTransformation.html">ShowDiffTransformation</a></li><li><a href="SpaceTimeLayout.html">SpaceTimeLayout</a></li><li><a href="StringLiteral.html">StringLiteral</a></li><li><a href="TextQueryMotifFinder.html">TextQueryMotifFinder</a></li><li><a href="Token.html">Token</a></li><li><a href="TokenType.html">TokenType</a></li><li><a href="Transformation.html">Transformation</a></li><li><a href="Transformer.html">Transformer</a></li><li><a href="Util.html">Util</a></li><li><a href="VectorTimestamp.html">VectorTimestamp</a></li><li><a href="VectorTimestampSerializer.html">VectorTimestampSerializer</a></li><li><a href="View.html">View</a></li><li><a href="VisualEdge.html">VisualEdge</a></li><li><a href="VisualGraph.html">VisualGraph</a></li><li><a href="VisualNode.html">VisualNode</a></li></ul><h3>Global</h3><ul><li><a href="global.html#Line">Line</a></li></ul>
</nav>

<br clear="both">

<footer>
    Documentation generated by <a href="https://github.com/jsdoc3/jsdoc">JSDoc 3.3.0-dev</a> on Sun Apr 26 2015 22:55:54 GMT-0700 (PDT)
</footer>

<script> prettyPrint(); </script>
<script src="scripts/linenumber.js"> </script>
</body>
</html>
