<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JSDoc: Source: transform/transformer.js</title>

    <script src="scripts/prettify/prettify.js"> </script>
    <script src="scripts/prettify/lang-css.js"> </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="styles/prettify-tomorrow.css">
    <link type="text/css" rel="stylesheet" href="styles/jsdoc-default.css">
</head>

<body>

<div id="main">

    <h1 class="page-title">Source: transform/transformer.js</h1>

    


    
    <section>
        <article>
            <pre class="prettyprint source linenums"><code>/**
 * Constructs a Transformer
 * 
 * @classdesc
 * 
 * &lt;p>
 * A Transformer is responsible for transforming a {@link VisualGraph} and the
 * underlying {@link ModelGraph}. Transformer exposes methods such as
 * {@link Transformer#hideHost} that allow other classes to specify how this
 * transformer should transform graphs. Internally, it manages a bunch of
 * {@link Transformation}s to achieve the desired effect.
 * &lt;/p>
 * 
 * &lt;p>
 * Shiviz {@link Transformation}s can interact with each other in complex ways.
 * For this reason, Transformer does not expose Transformations to outside
 * classes, but instead acts as a black box that can be used to transform graphs
 * &lt;/p>
 * 
 * &lt;p>
 * Typical usage for this class involves invoking methods such as
 * {@link Transformer#collapseNode} to specifiy how the graph should be
 * transformed and then invoking the {@link Transformer#transform} method. Note
 * that no graphs or visual graphs are actually modified until the transform
 * method is called.
 * &lt;/p>
 * 
 * @constructor
 */
function Transformer() {

    /** @private */
    this.transformations = [];

    /** @private */
    this.hostToHidingTransform = {};
	
    /** @private */
    this.viewToDiffTransform = {};

    /** @private */
    this.collapseSequentialNodesTransformation = new CollapseSequentialNodesTransformation(2);

    /** @private */
    this.highlightHostTransformation = new HighlightHostTransformation();

    /** @private */
    this.highlightHostToIndex = {};

    /** @private */
    this.highlightMotifTransformation = null;

    /** @private */
    this.hiddenHosts = [];
	
    /** @private */
    this.uniqueHosts = [];
	
    /** @private */
    this.uniqueEvents = [];

    /** @private */
    this.highlighted = null;

}

/**
 * Sets this transformer to hide the specified host. If the specified host is
 * already set to be hidden, this method does nothing.
 * 
 * @param {String} host The host that is to be hidden
 * @see {@link HideHostTransformation}
 */
Transformer.prototype.hideHost = function(host) {
    if (this.hostToHidingTransform[host]) {
        return;
    }
    var trans = new HideHostTransformation(host);
    this.hostToHidingTransform[host] = trans;
    this.transformations.push(trans);
};

/**
 * &lt;p>
 * Unsets this transformer to hide the specified host. If the specified host is
 * not currently set to be hidden, this method does nothing.
 * &lt;/p>
 * 
 * &lt;p>
 * A host could've been hidden either explicitly with a call to
 * {@link Transformer#hideHost} or implicitly when a host is highlighted. In the
 * latter case, all highlighted nodes will be unhighlighted as well.
 * &lt;/p>
 * 
 * @param {String} host The host that is no longer to be hidden
 * @see {@link HideHostTransformation}
 */
Transformer.prototype.unhideHost = function(host) {
    var trans = this.hostToHidingTransform[host];
    if (trans) {
        var index = this.transformations.indexOf(trans);
        this.transformations.splice(index, 1);
        delete this.hostToHidingTransform[host];
    }
    else if (this.highlightHostTransformation.isHidden(host)) {
        this.highlightHostTransformation.clearHosts();
    }
};

/**
 * Gets the hosts that are explicitly specified to be hidden. Note that a hosts
 * that doesn't actually exist can be specified to be hidden. In addition, there
 * are hosts that may be implicitly hidden by other transformations. These two
 * facts mean that the returned list of hosts may be different from the hosts
 * that are actually hidden.
 * 
 * @returns {Array&lt;String>} the hosts specified to be hidden
 * @see {@link HideHostTransformation}
 */
Transformer.prototype.getSpecifiedHiddenHosts = function() {
    return Object.keys(this.hostToHidingTransform);
};

/**
 * Get all of the hosts hidden by this transformer by the last invocation of
 * {@link Transformer#transform}. If the transform method has never been
 * called, this method returns an empty array.
 * 
 * @returns {Array&lt;String>} the hosts that have been hidden by this
 *          transformer.
 */
Transformer.prototype.getHiddenHosts = function() {
    return this.hiddenHosts.slice();
};

/**
 * Get all of the unique hosts hidden by this transformer by the last invocation of
 * {@link Transformer#transform}. If the transform method has never been
 * called, this method returns an empty array.
 * 
 * @returns {Array&lt;String>} the unique hosts that have been hidden by this
 *          transformer.
 */
Transformer.prototype.getUniqueHosts = function() {
    return this.uniqueHosts.slice();
};

/**
 * Get all of the unique events transformed by this transformer by the last invocation of
 * {@link Transformer#transform}. If the transform method has never been
 * called, this method returns an empty array.
 * 
 * @returns {Array&lt;String>} the unique events that have been transformed by this
 *          transformer.
 */
Transformer.prototype.getUniqueEvents = function() {
    return this.uniqueEvents.slice();
};

/**
 * Sets this transformer to highlight the specified host.
 * 
 * @param {String} host The host to be highlighted
 * @param {Boolean} def Whether the transformation to remove is a default
 *            transformation.
 * @see {@link HighlightHostTransformation}
 */
Transformer.prototype.highlightHost = function(host) {
    this.highlightHostTransformation.addHost(host);
    this.highlightHostToIndex[host] = this.transformations.length;
};

/**
 * Unsets this transformer to highlight the specified host.
 * 
 * @param {String} host The host that is no longer to be highlighted
 * @see {@link HighlightHostTransformation}
 */
Transformer.prototype.unhighlighHost = function(host) {
    this.highlightHostTransformation.removeHost(host);
    delete this.highlightHostToIndex[host];
};

/**
 * Toggles highlighting of the specified host.
 * 
 * @param {String} host
 * @see {@link HighlightHostTransformation}
 */
Transformer.prototype.toggleHighlightHost = function(host) {
    if (this.highlightHostTransformation.isHighlighted(host)) {
        this.unhighlighHost(host);
    }
    else {
        this.highlightHost(host);
    }
};

/**
 * Sets this transformer to collapse the node and its group into one.
 * Intuitively, the node's group are the nodes surrounding the argument that
 * have no family.
 * 
 * @param {ModelNode} node
 * @see {@link CollapseSequentialNodesTransformation}
 */
Transformer.prototype.collapseNode = function(node) {
    this.collapseSequentialNodesTransformation.removeExemption(node);
};

/**
 * Sets this transformer to not collapse the node or any of the nodes in its
 * group. Intuitively, the node's group are the nodes surrounding the argument
 * that have no family.
 * 
 * @param {ModelNode} node
 * @see {@link CollapseSequentialNodesTransformation}
 */
Transformer.prototype.uncollapseNode = function(node) {
    this.collapseSequentialNodesTransformation.addExemption(node);
};

/**
 * Toggles collapsing of the node
 * 
 * @param {ModelNode} node
 * @see {@link CollapseSequentialNodesTransformation}
 */
Transformer.prototype.toggleCollapseNode = function(node) {
    this.collapseSequentialNodesTransformation.toggleExemption(node);
};

/**
 * Sets this transformer to highlight a motif found by a MotifFinder. Only one
 * motif can be highlighted at a time, thus if there is already a motif set to
 * be highlighted, that one is replaced.
 * 
 * @param {MotifFinder} motifFinder The motif finder that specifies which nodes
 *            and edges are to be highlighted
 * @param {Boolean} ignoreEdges edges will not be highlighted if true
 * @see {@link HighlightMotifTransformation}
 */
Transformer.prototype.highlightMotif = function(motifFinder, ignoreEdges) {
    this.highlightMotifTransformation = new HighlightMotifTransformation(motifFinder, ignoreEdges);
};

/**
 * Sets this transformer to not highlight motifs.
 * 
 * @see {@link HighlightMotifTransformation}
 */
Transformer.prototype.unhighlightMotif = function() {
    this.highlightMotifTransformation = null;
};

/**
 * Determines if a motif is currently set to be highlighted.
 * 
 * @returns {Boolean} True if a motif is currently set to be highlighted
 * @see {@link HighlightMotifTransformation}
 */
Transformer.prototype.hasHighlightedMotif = function() {
    return this.highlightMotifTransformation != null;
};

/**
 * Returns the motif group that represents the highlighted elements from the
 * last invocation of {@link Transformer#transform}. If transform has yet to be
 * called, this method returns null
 * 
 * @returns {MotifGroup}
 * @see {@link HighlightMotifTransformation}
 */
Transformer.prototype.getHighlightedMotif = function() {
    return this.highlighted;
};

/**
 * Sets this transformer to highlight different hosts in the View
 * this transformer belongs to and the given View passed to the function
 */
 Transformer.prototype.showDiff = function(view) {
    if (this.viewToDiffTransform[view]) return;
    while (this.uniqueHosts.length) { this.uniqueHosts.pop(); }
	
    var trans = new ShowDiffTransformation(view, this.uniqueHosts, this.hiddenHosts, this.uniqueEvents);
    this.viewToDiffTransform[view] = trans;
    this.transformations.push(trans);
}

/**
 * Sets this transformer to not highlight different hosts 
 */
 Transformer.prototype.hideDiff = function(view) {
    var trans = this.viewToDiffTransform[view];
    if (trans) {
       var index = this.transformations.indexOf(trans);
       this.transformations.splice(index, 1);
       delete this.viewToDiffTransform[view];
       // empty uniqueHosts array so that hosts in sidebar return to rectangle shapes
       while (this.uniqueHosts.length) { this.uniqueHosts.pop(); }
       while (this.uniqueEvents.length) { this.uniqueEvents.pop(); }
    }
}

/**
 * Transforms the specified {@link VisualGraph} and the underlying
 * {@link ModelGraph} based on the settings of this transformer. Note that this
 * method is solely responsible for modifying visual and model graphs
 */
Transformer.prototype.transform = function(visualModel) {
    var originalHosts = visualModel.getHosts();
    // get the underlying modelGraph
    var graph = visualModel.getGraph();

    this.collapseSequentialNodesTransformation.transform(visualModel);

    var maxIndex = 0;
    for (var key in this.highlightHostToIndex) {
        maxIndex = Math.max(maxIndex, this.highlightHostToIndex[key]);
    }

    for (var i = 0; i &lt; maxIndex; i++) {
        var trans = this.transformations[i];
        trans.transform(visualModel);
    }

    this.highlightHostTransformation.transform(visualModel);

    for (var i = maxIndex; i &lt; this.transformations.length; i++) {
        var trans = this.transformations[i];
        trans.transform(visualModel);
    }

    if (this.highlightMotifTransformation != null) {
        this.highlightMotifTransformation.transform(visualModel);
        this.highlighted = this.highlightMotifTransformation.getHighlighted();
    }

    var hidden = {};
    for (var i = 0; i &lt; originalHosts.length; i++) {
        var host = originalHosts[i];
        if (this.hostToHidingTransform[host]) {
            hidden[host] = true;
        }
    }

    this.highlightHostTransformation.getHiddenHosts().forEach(function(host) {
        hidden[host] = true;
    });

    this.hiddenHosts = Object.keys(hidden);
};</code></pre>
        </article>
    </section>




</div>

<nav>
    <h2><a href="index.html">Index</a></h2><h3>Classes</h3><ul><li><a href="AbstractGraph.html">AbstractGraph</a></li><li><a href="AbstractNode.html">AbstractNode</a></li><li><a href="AddFamilyEvent.html">AddFamilyEvent</a></li><li><a href="AddNodeEvent.html">AddNodeEvent</a></li><li><a href="AST.html">AST</a></li><li><a href="BinaryOp.html">BinaryOp</a></li><li><a href="BroadcastGatherFinder.html">BroadcastGatherFinder</a></li><li><a href="BuilderGraph.html">BuilderGraph</a></li><li><a href="BuilderNode.html">BuilderNode</a></li><li><a href="ChangeEvent.html">ChangeEvent</a></li><li><a href="CollapseSequentialNodesTransformation.html">CollapseSequentialNodesTransformation</a></li><li><a href="Controller.html">Controller</a></li><li><a href="CustomMotifFinder.html">CustomMotifFinder</a></li><li><a href="DFSGraphTraversal.html">DFSGraphTraversal</a></li><li><a href="Exception.html">Exception</a></li><li><a href="Global_.html">Global</a></li><li><a href="GraphBuilder.html">GraphBuilder</a></li><li><a href="GraphBuilderHost.html">GraphBuilderHost</a></li><li><a href="GraphBuilderNode.html">GraphBuilderNode</a></li><li><a href="GraphTraversal.html">GraphTraversal</a></li><li><a href="HideHostTransformation.html">HideHostTransformation</a></li><li><a href="HighlightHostTransformation.html">HighlightHostTransformation</a></li><li><a href="HighlightMotifTransformation.html">HighlightMotifTransformation</a></li><li><a href="HostPermutation.html">HostPermutation</a></li><li><a href="Identifier.html">Identifier</a></li><li><a href="ImplicitSearch.html">ImplicitSearch</a></li><li><a href="Layout.html">Layout</a></li><li><a href="LEMInterpreter.html">LEMInterpreter</a></li><li><a href="LEMInterpreterValue.html">LEMInterpreterValue</a></li><li><a href="LEMParser.html">LEMParser</a></li><li><a href="LEMTokenizer.html">LEMTokenizer</a></li><li><a href="LengthPermutation.html">LengthPermutation</a></li><li><a href="LogEvent.html">LogEvent</a></li><li><a href="LogEventMatcher.html">LogEventMatcher</a></li><li><a href="LogOrderPermutation.html">LogOrderPermutation</a></li><li><a href="LogParser.html">LogParser</a></li><li><a href="ModelGraph.html">ModelGraph</a></li><li><a href="ModelNode.html">ModelNode</a></li><li><a href="Motif.html">Motif</a></li><li><a href="MotifFinder.html">MotifFinder</a></li><li><a href="MotifGroup.html">MotifGroup</a></li><li><a href="MotifNavigator.html">MotifNavigator</a></li><li><a href="MotifNavigatorData.html">MotifNavigatorData</a></li><li><a href="NamedRegExp.html">NamedRegExp</a></li><li><a href="RegexLiteral.html">RegexLiteral</a></li><li><a href="RemoveFamilyEvent.html">RemoveFamilyEvent</a></li><li><a href="RemoveHostEvent.html">RemoveHostEvent</a></li><li><a href="RemoveNodeEvent.html">RemoveNodeEvent</a></li><li><a href="RequestResponseFinder.html">RequestResponseFinder</a></li><li><a href="SearchBar.html">SearchBar</a></li><li><a href="Shiviz.html">Shiviz</a></li><li><a href="ShowDiffTransformation.html">ShowDiffTransformation</a></li><li><a href="SpaceTimeLayout.html">SpaceTimeLayout</a></li><li><a href="StringLiteral.html">StringLiteral</a></li><li><a href="TextQueryMotifFinder.html">TextQueryMotifFinder</a></li><li><a href="Token.html">Token</a></li><li><a href="TokenType.html">TokenType</a></li><li><a href="Transformation.html">Transformation</a></li><li><a href="Transformer.html">Transformer</a></li><li><a href="Util.html">Util</a></li><li><a href="VectorTimestamp.html">VectorTimestamp</a></li><li><a href="VectorTimestampSerializer.html">VectorTimestampSerializer</a></li><li><a href="View.html">View</a></li><li><a href="VisualEdge.html">VisualEdge</a></li><li><a href="VisualGraph.html">VisualGraph</a></li><li><a href="VisualNode.html">VisualNode</a></li></ul><h3>Global</h3><ul><li><a href="global.html#Line">Line</a></li></ul>
</nav>

<br clear="both">

<footer>
    Documentation generated by <a href="https://github.com/jsdoc3/jsdoc">JSDoc 3.3.0-dev</a> on Sun Apr 26 2015 22:55:54 GMT-0700 (PDT)
</footer>

<script> prettyPrint(); </script>
<script src="scripts/linenumber.js"> </script>
</body>
</html>
