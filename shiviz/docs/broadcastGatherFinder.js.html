<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>JSDoc: Source: motifFinder/broadcastGatherFinder.js</title>

    <script src="scripts/prettify/prettify.js"> </script>
    <script src="scripts/prettify/lang-css.js"> </script>
    <!--[if lt IE 9]>
      <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link type="text/css" rel="stylesheet" href="styles/prettify-tomorrow.css">
    <link type="text/css" rel="stylesheet" href="styles/jsdoc-default.css">
</head>

<body>

<div id="main">

    <h1 class="page-title">Source: motifFinder/broadcastGatherFinder.js</h1>

    


    
    <section>
        <article>
            <pre class="prettyprint source linenums"><code>/**
 * @classdesc
 * 
 * &lt;p>
 * This class is responsible for finding broadcast or gather motif.
 * &lt;/p>
 * 
 * &lt;p>
 * Intuitively, a broadcast motif is a communication pattern where a host sends
 * messages to other hosts in quick succession. A gather motif then is where
 * multiple hosts send messages to a single one in quick succession.
 * &lt;/p>
 * 
 * &lt;p>
 * More formally, a broadcast motif is sequence S of k nodes n_1, n_2, n_3 ...
 * n_k
 * &lt;ul>
 * &lt;li>S is a sequence of consecutive nodes&lt;/li>
 * &lt;li>No nodes in S except n_1 may have any parents. n_1 can have any number
 * of parents.&lt;/li>
 * &lt;li>Each node in T is the child of a node in S&lt;/li>
 * &lt;li>Define valid-parent as follows: a node n_i is a valid-parent if it is a
 * parent of a node n_c such that n_c's host is not equal to the host of any of
 * the children of nodes n_1 to n_(i-1) inclusive.
 * &lt;li>For all nodes n_i in S that are valid-parents let n_j be the node in S
 * with the smallest j such that j > i that is also a valid-parent. The number
 * of nodes between n_i and n_j must be less than or equal to the parameter
 * maxInBetween&lt;/li>
 * &lt;li>Let Hosts be the set of hosts of all nodes in the set of nodes that are
 * a child of a node in S. The cardinality of Hosts must be greater than or
 * equal to the parameter minBroadcastGather.
 * &lt;/ul>
 * &lt;/p>
 * 
 * &lt;p>
 * The actual motif itself comprises all nodes in S and all nodes that are
 * children of a node in S. It also contains all edges that connect any two
 * nodes in S and all edges that connect any node in S its children.
 * &lt;/p>
 * 
 * &lt;p>
 * The formal definition of a gather motif is analogous to broadcast. In the
 * formal definition above, replace child(ren) with parent(s) and parent(s) with
 * child(ren). One difference for gather is that no nodes in S INCLUDING n_1 may
 * have any parents.
 * &lt;/p>
 * 
 * @constructor
 * @extends MotifFinder
 * @param {int} minBroadcastGather Minimum amount of broadcasts or gathers
 *            needed to accept a motif
 * @param {int} maxInBetween Maximum number of non-broadcasting or gathering
 *            nodes allowed since previous broadcasting or gathering one. This
 *            number includes the last broadcasting or gathering node itself.
 *            For example, if you set this to 1, the finder will allow ZERO
 *            nodes between broadcast/gather nodes (in other words, ONE node
 *            since the previous broadcast/gather node including that one)
 * @param {Boolean} broadcast Set to true to find broadcasts. Set to false to
 *            find gathers
 */
function BroadcastGatherFinder(minBroadcastGather, maxInBetween, broadcast) {
    
    /** @private */
    this.minBroadcastGather = minBroadcastGather;
    
    /** @private */
    this.maxInBetween = maxInBetween;
    
    /** @private */
    this.broadcast = broadcast;
};

// BroadcastGatherFinder extends MotifFinder
BroadcastGatherFinder.prototype = Object.create(MotifFinder.prototype);
BroadcastGatherFinder.prototype.constructor = BroadcastGatherFinder;

/**
 * 
 * @private
 * @static
 */
BroadcastGatherFinder.GREEDY_THRESHOLD = 300;

/**
 * Overrides {@link MotifFinder#find}
 */
BroadcastGatherFinder.prototype.find = function(graph) {

    var context = this; // Used by inner functions

    var motifGroup = new MotifGroup();
    var disjoints = findDisjoint();

    for (var d = 0; d &lt; disjoints.length; d++) {
        var disjoint = disjoints[d];

        if (disjoint.length &lt;= BroadcastGatherFinder.GREEDY_THRESHOLD) {
            var score = findAll(disjoint);
            var groups = findBestGroups(disjoint, score);
            motifGroup.addMotif(toMotif(groups));
        }
        else {
            findGreedy(disjoint, finalMotif);
        }
    }

    return motifGroup;

    // Finds disjoint groups of potential broadcasts/gathers
    function findDisjoint() {
        var ret = [];

        var hosts = graph.getHosts();
        for (var h = 0; h &lt; hosts.length; h++) {
            var host = hosts[h];
            var group = []; // The current disjoint group
            var inBetween = 0;
            var inPattern = false;

            var curr = graph.getHead(host).getNext();
            while (curr != null) {

                var hasBadLink = context.broadcast ? curr.hasParents() : curr.hasChildren();
                var hasGoodLink = context.broadcast ? curr.hasChildren() : curr.hasParents();

                // If curr can't be part of the current group, push group and
                // start a new group for curr
                if (inBetween > context.maxInBetween || curr.isTail() || hasBadLink) {
                    if (inPattern &amp;&amp; group.length != 0) {
                        ret.push(group);
                        group = [];
                        inPattern = false;
                    }

                }

                if (hasGoodLink) {
                    // if not currently in broadcast/gather pattern, clear group
                    if (!inPattern) {
                        inPattern = true;
                        group = [];
                    }
                    inBetween = 1 - curr.getLogEventCount();
                }

                group.push(curr);
                inBetween += curr.getLogEventCount();
                curr = curr.getNext();
            }
        }
        return ret;
    }

    /*
     * Finds a broadcasts/gathers within the group. O(n^2) returns score array
     * where score[i][j] = number of broadcasts between ith and jth node in
     * group inclusive ONLY IF they form a valid broadcast motif
     */
    function findAll(group) {

        var score = [];

        for (var i = 0; i &lt; group.length; i++) {

            var count = 0; // current broadcast/gather count
            var inBetween = 0; // Nodes since last valid broadcasting/gathering
            // node
            var seenHosts = {};
            score[i] = [];

            for (var j = i; j &lt; group.length; j++) {
                var curr = group[j];

                // Check if curr has a valid link (a link to a host that hasn't
                // been seen yet)
                var hasValidLink = false;
                var links = context.broadcast ? curr.getChildren() : curr.getParents();
                for (var k = 0; k &lt; links.length; k++) {
                    if (!seenHosts[links[k].getHost()]) {
                        hasValidLink = true;
                        break;
                    }
                }

                var hasBadLink = context.broadcast ? curr.hasParents() : curr.hasChildren();
                var hasGoodLink = context.broadcast ? curr.hasChildren() : curr.hasParents();
                // we allow parent links for the first node of a broadcast
                var allowBadLink = context.broadcast &amp;&amp; j == i;

                // (hasGoodLink &amp;&amp; !hasValidLink) == has children/parents, but
                // all of them are to hosts already seen
                if (inBetween > context.maxInBetween || (hasBadLink &amp;&amp; !allowBadLink) || (hasGoodLink &amp;&amp; !hasValidLink)) {
                    break;
                }

                if (hasGoodLink) {
                    for (var k = 0; k &lt; links.length; k++) {
                        var childHost = links[k].getHost();
                        if (!seenHosts[childHost]) {
                            count++;
                            seenHosts[childHost] = true;
                        }
                    }
                    inBetween = 1 - curr.getLogEventCount();
                    score[i][j] = count;
                }

                inBetween += curr.getLogEventCount();
            }
        }

        return score;
    }

    // Finds the best partition of nodes into broadcast motifs
    function findBestGroups(nodes, score) {
        var best = []; // best[i] = max score using nodes 0 to i inclusive
        var parent = [];

        // Find max score using O(n^2) dynamic programming
        // dp recurrence: best[i] = max(best[j-1] + score[j][i]) for all 0&lt;=j&lt;=i
        for (var i = 0; i &lt; nodes.length; i++) {
            var max = -1;
            for (var j = 0; j &lt;= i; j++) {
                var newScore = 0;
                var ownScore = score[j][i];
                if (!!ownScore &amp;&amp; ownScore >= context.minBroadcastGather) {
                    newScore += ownScore;
                }
                if (j > 0) {
                    newScore += best[j - 1];
                }
                if (newScore > max) {
                    max = newScore;
                    parent[i] = j - 1;
                }
            }
            best[i] = max;
        }

        // backtrack dp to recover the actual groups of nodes
        var groups = [];
        var loc = nodes.length - 1;
        while (loc != -1) {
            var ploc = parent[loc];
            var groupStart = nodes[ploc + 1];
            var groupEnd = nodes[loc];
            var currScore = score[ploc + 1][loc];
            if (!!currScore &amp;&amp; currScore >= context.minBroadcastGather) {
                groups.push([ groupStart, groupEnd ]);
            }
            loc = parent[loc];
        }

        return groups;
    }

    // Creates a motif out of the groups
    function toMotif(groups) {
        var motif = new Motif();
        
        for (var j = 0; j &lt; groups.length; j++) {
            var curr = groups[j][0];
            var groupEnd = groups[j][1].getNext();
            var prev = null;
            var seenHosts = {};

            while (curr != groupEnd) {
                motif.addNode(curr);
                if (prev != null) {
                    motif.addEdge(curr, prev);
                }

                var links = context.broadcast ? curr.getChildren() : curr.getParents();
                for (var i = 0; i &lt; links.length; i++) {
                    motif.addEdge(curr, links[i]);
                    motif.addNode(links[i]);
                    var linkHost = links[i].getHost();
                    if (!seenHosts[linkHost]) {
                        seenHosts[linkHost] = true;
                    }
                }

                prev = curr;
                curr = curr.getNext();
            }
        }
        
        return motif;
    }

    // Alternate greedy solution. Used when O(n^2) dp is too slow
    function findGreedy(group, motif) {
        var bcCount = 0;
        var inBetween = 0;
        var inPattern = false;
        var currMotif = new Motif();
        var queued = [];
        var nodes = [];
        var seenHosts = {};
        group = group.concat([ new ModelNode([]) ]);

        for (var g = 0; g &lt; group.length; g++) {
            var curr = group[g];
            queued.push(curr);

            var hasValidLink = false;
            var links = context.broadcast ? curr.getChildren() : curr.getParents();
            for (var i = 0; i &lt; links.length; i++) {
                if (!seenHosts[links[i].getHost()]) {
                    hasValidLink = true;
                    break;
                }
            }

            var hasBadLink = context.broadcast ? curr.hasParents() : curr.hasChildren();
            var hasGoodLink = context.broadcast ? curr.hasChildren() : curr.hasParents();

            if (inBetween > context.maxInBetween || (g == group.length - 1) || hasBadLink || (hasGoodLink &amp;&amp; !hasValidLink)) {
                if (bcCount >= context.minBroadcastGather) {
                    for (var i = 1; i &lt; nodes.length; i++) {
                        currMotif.addEdge(nodes[i - 1], nodes[i]);
                    }
                    motif.merge(currMotif);
                }
                inPattern = false;
            }

            if (hasGoodLink &amp;&amp; (context.broadcast || !hasBadLink)) {
                if (!inPattern) {
                    inPattern = true;
                    bcCount = 0;
                    inBetween = 0;
                    currMotif = new Motif();
                    queued = [ curr ];
                    seenHosts = {};
                    nodes = [];
                }

                currMotif.addAllNodes(links);
                for (var i = 0; i &lt; links.length; i++) {
                    currMotif.addEdge(curr, links[i]);
                    var linkHost = links[i].getHost();
                    if (!seenHosts[linkHost]) {
                        bcCount++;
                        seenHosts[linkHost] = true;
                    }
                }
                currMotif.addAllNodes(queued);
                nodes = nodes.concat(queued);
                queued = [];
                inBetween = 1 - curr.getLogEventCount();
            }

            inBetween += curr.getLogEventCount();
        }
    }

};
</code></pre>
        </article>
    </section>




</div>

<nav>
    <h2><a href="index.html">Index</a></h2><h3>Classes</h3><ul><li><a href="AbstractGraph.html">AbstractGraph</a></li><li><a href="AbstractNode.html">AbstractNode</a></li><li><a href="AddFamilyEvent.html">AddFamilyEvent</a></li><li><a href="AddNodeEvent.html">AddNodeEvent</a></li><li><a href="AST.html">AST</a></li><li><a href="BinaryOp.html">BinaryOp</a></li><li><a href="BroadcastGatherFinder.html">BroadcastGatherFinder</a></li><li><a href="BuilderGraph.html">BuilderGraph</a></li><li><a href="BuilderNode.html">BuilderNode</a></li><li><a href="ChangeEvent.html">ChangeEvent</a></li><li><a href="CollapseSequentialNodesTransformation.html">CollapseSequentialNodesTransformation</a></li><li><a href="Controller.html">Controller</a></li><li><a href="CustomMotifFinder.html">CustomMotifFinder</a></li><li><a href="DFSGraphTraversal.html">DFSGraphTraversal</a></li><li><a href="Exception.html">Exception</a></li><li><a href="Global_.html">Global</a></li><li><a href="GraphBuilder.html">GraphBuilder</a></li><li><a href="GraphBuilderHost.html">GraphBuilderHost</a></li><li><a href="GraphBuilderNode.html">GraphBuilderNode</a></li><li><a href="GraphTraversal.html">GraphTraversal</a></li><li><a href="HideHostTransformation.html">HideHostTransformation</a></li><li><a href="HighlightHostTransformation.html">HighlightHostTransformation</a></li><li><a href="HighlightMotifTransformation.html">HighlightMotifTransformation</a></li><li><a href="HostPermutation.html">HostPermutation</a></li><li><a href="Identifier.html">Identifier</a></li><li><a href="ImplicitSearch.html">ImplicitSearch</a></li><li><a href="Layout.html">Layout</a></li><li><a href="LEMInterpreter.html">LEMInterpreter</a></li><li><a href="LEMInterpreterValue.html">LEMInterpreterValue</a></li><li><a href="LEMParser.html">LEMParser</a></li><li><a href="LEMTokenizer.html">LEMTokenizer</a></li><li><a href="LengthPermutation.html">LengthPermutation</a></li><li><a href="LogEvent.html">LogEvent</a></li><li><a href="LogEventMatcher.html">LogEventMatcher</a></li><li><a href="LogOrderPermutation.html">LogOrderPermutation</a></li><li><a href="LogParser.html">LogParser</a></li><li><a href="ModelGraph.html">ModelGraph</a></li><li><a href="ModelNode.html">ModelNode</a></li><li><a href="Motif.html">Motif</a></li><li><a href="MotifFinder.html">MotifFinder</a></li><li><a href="MotifGroup.html">MotifGroup</a></li><li><a href="MotifNavigator.html">MotifNavigator</a></li><li><a href="MotifNavigatorData.html">MotifNavigatorData</a></li><li><a href="NamedRegExp.html">NamedRegExp</a></li><li><a href="RegexLiteral.html">RegexLiteral</a></li><li><a href="RemoveFamilyEvent.html">RemoveFamilyEvent</a></li><li><a href="RemoveHostEvent.html">RemoveHostEvent</a></li><li><a href="RemoveNodeEvent.html">RemoveNodeEvent</a></li><li><a href="RequestResponseFinder.html">RequestResponseFinder</a></li><li><a href="SearchBar.html">SearchBar</a></li><li><a href="Shiviz.html">Shiviz</a></li><li><a href="ShowDiffTransformation.html">ShowDiffTransformation</a></li><li><a href="SpaceTimeLayout.html">SpaceTimeLayout</a></li><li><a href="StringLiteral.html">StringLiteral</a></li><li><a href="TextQueryMotifFinder.html">TextQueryMotifFinder</a></li><li><a href="Token.html">Token</a></li><li><a href="TokenType.html">TokenType</a></li><li><a href="Transformation.html">Transformation</a></li><li><a href="Transformer.html">Transformer</a></li><li><a href="Util.html">Util</a></li><li><a href="VectorTimestamp.html">VectorTimestamp</a></li><li><a href="VectorTimestampSerializer.html">VectorTimestampSerializer</a></li><li><a href="View.html">View</a></li><li><a href="VisualEdge.html">VisualEdge</a></li><li><a href="VisualGraph.html">VisualGraph</a></li><li><a href="VisualNode.html">VisualNode</a></li></ul><h3>Global</h3><ul><li><a href="global.html#Line">Line</a></li></ul>
</nav>

<br clear="both">

<footer>
    Documentation generated by <a href="https://github.com/jsdoc3/jsdoc">JSDoc 3.3.0-dev</a> on Sun Apr 26 2015 22:55:54 GMT-0700 (PDT)
</footer>

<script> prettyPrint(); </script>
<script src="scripts/linenumber.js"> </script>
</body>
</html>
