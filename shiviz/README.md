ShiViz is a tool for studying executions of distributed systems.

* [**Try the tool!**](http://bestchai.bitbucket.org/shiviz/)

* [Wiki](https://bitbucket.org/bestchai/shiviz/wiki/): explains ShiViz in more detail

* [JS docs](http://bestchai.bitbucket.org/shiviz/docs/): documentation extracted from code

* [ShiVector](https://bitbucket.org/bestchai/shivector/wiki/): instruments Java programs to output logs in ShiViz format

* [GoVector](https://github.com/arcaneiceman/GoVector): instruments Go programs to output logs in ShiViz format